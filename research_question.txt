Group: group 34

Question
========

RQ: Is there any corelation between Height of a person and Lung Capacity?

Null hypothesis: There is no relation between the Height of a person and Lung Capacity.

Alternative hypothesis: There is relation between the Height of a person and Lung Capacity.


Dataset
=======

URL: https://www.kaggle.com/aakashmisra/lung-capacity

Column Headings:

```
> lcd <- read.csv("LungCapData.csv", header = TRUE)
> colnames(lcd)
[1] "LungCap"   "Age"       "Height"    "Smoke"     "Gender"    "Caesarean"
>                                    

```
